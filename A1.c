/*
compile using: gcc A1.c
then to run
./a.out [input] [number of threads]


*/
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sched.h>

void initializeArrays();
void print_array(int **x);
void generateNext(void *p);
int live_neighbours(int **x, int i, int j);
void gen(int **curr, int **next, int num, int k);

int N;
int **gridA;//odd generations
int **gridB;//even
int toggle;
int T;

int main ( int argc, char *argv[] ){
    int i,j;
    const int STACK_SIZE = 65536;

	if(argc != 3)
	{
		fprintf(stderr, "Two arguments needed. Proper Usage: [file, T_thread_count]");
		printf("\n");
		exit(1);
	}

	FILE *file = fopen(argv[1], "r");
	int t = atoi(argv[2]);

    char *stack;
    char *stackTop[t];
    pid_t cret[t], wret;
    
    T = t;
    fscanf(file,"%d",&N);//set N
    initializeArrays();

	if (t > N){ fprintf (stderr, "T > N number of threads exceeds capacity"); exit(-1); }

    int status;

    //put all input numbers in array
    while(!feof(file)){
        for(i = 0; i < N; i++)
            for(j = 0; j < N; j++)
                fscanf(file,"%d",&gridA[i][j]);
    }
    
    fclose( file );
    
    print_array(gridA);//print first generation from input file
    
	char value [80];
    toggle = 0;//toggle to switch between gridA and gridB// 0 = ( cur=b | nxt = a)
    while(1) {
   scanf("%c",value);

		//infinite loop
        //program switches between grids
        //eg: calculations done on gridA using gridB and then printed, then next iteration reversed

        for(i=0; i < T; i++) {
            stack = malloc(STACK_SIZE);
            if(stack==NULL){
                perror("malloc"); exit(1);
            }
            stackTop[i] = stack + STACK_SIZE;
           cret[i] = clone(generateNext, stackTop[i], CLONE_VM|SIGCHLD, (void*)i);
        }
        for(i=0; i < 3; i++){
            waitpid(-1,0,0);
        }
        free(stack);
        if(toggle){
            print_array(gridA);
            toggle = 0;
        } else {
            print_array(gridB);
            toggle = 1;
        }
        sleep(1);
    }
}


//figures out the next generation
void generateNext(void *p){
    int q = (int) p;
    int **curr;
    int **next;
    
    int num = 0;
    int start = 0;
    
    if(q==0){
        num = N/T + N%T;
        
    } else {
        num = N/T*(q+1) + N%T;
        start = num - N/T;
    }

    if(toggle==1){
        gen(gridB,gridA,num,start);
    } else {
        gen(gridA,gridB,num,start);
    }

    _exit(0); 
}

//finds the neighbours and applys the rules
void gen(int **curr, int **next, int num, int k){
    
    int i = k;
    int j = k;
    //printf(" %d and %d curr array:", k, num);
    //algorithim for conways rules
    for(i = k;i < num; i ++){
        for(j = 0;j < N; j++){
            
            if(curr[i][j]==1){
                if(live_neighbours(curr, i, j) < 2 || live_neighbours(curr, i, j) > 3){
                    next[i][j] = 0; // dies
                    
                }else{
                    next[i][j] = 1;
                }
            } else if(live_neighbours(curr, i, j)==3){
                next[i][j] = 1;
            } else {next[i][j] = 0;}//default
		}
    }
}



//returns the amount of live neightbours
int live_neighbours(int **p, int x, int y){
    int count = 0;
    int i,j;
    int xPlus = x + 1;
    int xMinus = x - 1;
    int yPlus = y + 1;
    int yMinus = y - 1;
    
    //Does the wrapping stuff: if neighbor is bigger then N then wrap around etc
    if (xMinus < 0){
        xMinus = xMinus + N;
    }
    if (xPlus >= N){
        xPlus = N - xPlus;
    }
    if (yMinus < 0){
        yMinus = yMinus + N;
    }
    if (yPlus >= N){
        yPlus = N - yPlus;
    }
    
    //checks if neighbor is live, if so then increment count
    if(p[xMinus][yMinus] == 1) count++;
    if(p[xMinus][y] == 1) count++;
    if(p[xMinus][yPlus] == 1) count++;
    if(p[x][yMinus] == 1) count++;
    if(p[x][yPlus] == 1) count++;
    if(p[xPlus][yMinus] == 1) count++;
    if(p[xPlus][y] == 1) count++;
    if(p[xPlus][yPlus] == 1) count++;
    
    
    
    return count;
}



//prints the generation
void print_array(int **x){
    system("clear");
    int i,j,s;
    
    //prints top bars "-----"
    for(s = 0; s < N + 2; s++)
        printf("-");
    printf("\n");
    
    //prints the "|" border on the sides and replaces 0 with " " and 1 with "x"
    for(i = 0; i < N; i++){
        for(j = 0; j < N; j++){
            if(j==0)
                printf("|");
            
            if(x[i][j]==0)
                printf(" ");
            else
                printf("x");
            
            if(j==N-1)
                printf("|");
        }
        printf("\n");
    }
    
    //prints bottom bar "-----"
    for(s = 0; s < N + 2; s++)
        printf("-");
    printf("\n");
}

//initializes the arrays by making space on the heap
void initializeArrays(){
    int i;
    gridA = malloc(N * sizeof(int *));
    for (i = 0; i < N; i++)
        gridA[i] = malloc(N * sizeof(int));
    gridB = malloc(N * sizeof(int *));
    for (i = 0; i < N; i++)
        gridB[i] = malloc(N * sizeof(int));
}


